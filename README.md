CoAid web app:

As covid-19 spreads, more and more people are affected by the outcoming crisis not only medically, but also economically and psychologically. This site is providing information about points of interest where vulnerable groups of people can find support during the crisis. Points of interest could be about clothing, feeding, medical checking and other helpful activities targeting people in need.

You can check the deployed project here:
https://cnc-wp1.ellak.gr/2020cAutumnProjects/StaySafeStayHealthy/ProjectGroup1Morning/coAid/index.html

